#ifndef MIDTERM_ATTACK_H
#define MIDTERM_ATTACK_H

#include "approximation.h"

// performs a linear cryptanalysis attacks for K5
extern uint32_t attack_K5(approx_t approx, const uint16_t (*pairs)[2], int count);

// performs a linear cryptanalysis attacks for K1
extern uint32_t attack_K1(approx_t approx, const uint16_t (*pairs)[2], int count);

#endif //MIDTERM_ATTACK_H
