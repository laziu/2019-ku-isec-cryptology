#include "crypt.h"

// 4bit S-Box values
const uint8_t s_box[0x10] = {
        0xE, 0x4, 0xD, 0x1, 0x2, 0xF, 0xB, 0x8,
        0x3, 0xA, 0x6, 0xC, 0x5, 0x9, 0x0, 0x7
};

// Permutation indices
const uint8_t p_box[0x10] = {
        0x0, 0x4, 0x8, 0xC, 0x1, 0x5, 0x9, 0xD,
        0x2, 0x6, 0xA, 0xE, 0x3, 0x7, 0xB, 0xF
};

uint8_t inv_s_box[0x10];
uint8_t inv_p_box[0x10];

// encrypt 16bit block with 80bit key.
uint16_t encrypt(uint16_t blk, const uint16_t key[5]) {
    for (int i = 0; i <= 2; ++i) {
        blk ^= key[i]; // subkey K1~3 mixing
        blk = substitute(blk, false);
        blk = permute(blk, false);
    }
    blk ^= key[3]; // subkey K4 mixing
    blk = substitute(blk, false);
    blk ^= key[4]; // subkey K5 mixing
    return blk;
}

// decrypt 16bit block with 80bit key.
uint16_t decrypt(uint16_t blk, const uint16_t key[5]) {
    blk ^= key[4]; // subkey K5 mixing
    blk = substitute(blk, true);
    blk ^= key[3]; // subkey K4 mixing
    for (int i = 2; i >= 0; --i) {
        blk = permute(blk, true);
        blk = substitute(blk, true);
        blk ^= key[i]; // subkey K3~1 mixing
    }
    return blk;
}
