#ifndef MIDTERM_CRYPT_H
#define MIDTERM_CRYPT_H

#include <stdint.h>
#include <stdbool.h>

// 4bit S-Box values
extern const uint8_t s_box[0x10];

// Permutation indices
extern const uint8_t p_box[0x10];

// inverse S/P-box with initialization function
extern uint8_t inv_s_box[0x10];
extern uint8_t inv_p_box[0x10];

static inline void calc_inverses(void) {
    for (int i = 0; i <= 0xF; ++i)
        inv_s_box[s_box[i]] = i;
    for (int i = 0; i <= 0xF; ++i)
        inv_p_box[p_box[i]] = i;
}

// block substitution
static inline uint16_t substitute(uint16_t blk, bool inverted) {
    return ((inverted ? inv_s_box : s_box)[(blk) & 0xF]) |
           ((inverted ? inv_s_box : s_box)[(blk >> 0x4) & 0xF] << 0x4) |
           ((inverted ? inv_s_box : s_box)[(blk >> 0x8) & 0xF] << 0x8) |
           ((inverted ? inv_s_box : s_box)[(blk >> 0xC) & 0xF] << 0xC);
}

// block permutation
static inline uint16_t permute(uint16_t blk, bool inverted) {
    uint16_t res = 0;
    for (int i = 0; i < 0x10; ++i)
        res |= ((blk >> i) & 0x1) << (inverted ? inv_p_box : p_box)[i];  // okay because p-box is symmetric
    return res;
}

// encrypt/decrypt 16bit block with 80bit key.
extern uint16_t encrypt(uint16_t blk, const uint16_t key[5]);

extern uint16_t decrypt(uint16_t blk, const uint16_t key[5]);

// load pairs from file
extern bool load_pairs(const char *filename, uint16_t (*pairs)[2], int count, const uint16_t key[5]);

// save pairs to file
extern void save_pairs(const char *filename, const uint16_t (*pairs)[2], int count, const uint16_t key[5]);

#endif //MIDTERM_CRYPT_H
