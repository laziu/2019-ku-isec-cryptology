#include "crypt.h"
#include "approximation.h"
#include <stdio.h>
#include <stdlib.h>

// global parameter holder
typedef struct global_parameter_t {
    approx_t *__restrict full_approx;
    int full_approx_size;
    int full_approx_count;
    const int8_t (*__restrict sbox_approx)[0x10];
    double cut_bias;
    bool findK1;
    uint16_t p;
} global_parameter_t;

// parameter holder for round_assume
typedef struct round_parameter_t {
    int round;
    uint16_t u;
    double bias;
} round_parameter_t;

// parameter holder for sbox_assume
typedef struct side_parameter_t {
    int side;
    uint16_t sub_v;
    double prev_bias;
} side_parameter_t;

static void sbox_assume(global_parameter_t *__restrict gp, round_parameter_t *__restrict rp, side_parameter_t sp);

// each round step
static void round_assume(global_parameter_t *__restrict gp, round_parameter_t rp) {
    switch (rp.round) {
        case 2:
        case 3: {
            // permute after round 1, 2
            rp.u = permute(rp.u, gp->findK1);
        }
        case 1: {
            side_parameter_t s_param = { .side = 1, .sub_v = 0, .prev_bias = rp.bias };
            // select S-Box approximations of each round
            sbox_assume(gp, &rp, s_param);
            break;
        }
        case 4: {
            // permute after round 3
            rp.u = permute(rp.u, false);
            // if reached here, linear approximation bias will be greater than cut_bias; add it to result
            if (gp->findK1) {
                printf("Found Linear Approximation for K1 <- %04x:%04X (bias=%lf)\n", gp->p, rp.u, rp.bias);
                if (gp->full_approx_count < gp->full_approx_size) {
                    approx_t apx = { .x_mask = rp.u, .y_mask = gp->p, .bias = rp.bias };
                    gp->full_approx[gp->full_approx_count++] = apx;
                }
            } else {
                printf("Found Linear Approximation for K5 <- %04x:%04X (bias=%lf)\n", gp->p, rp.u, rp.bias);
                if (gp->full_approx_count < gp->full_approx_size) {
                    approx_t apx = { .x_mask = gp->p, .y_mask = rp.u, .bias = rp.bias };
                    gp->full_approx[gp->full_approx_count++] = apx;
                }
            }
            break;
        }
    }
}

// each S-box step
static void sbox_assume(global_parameter_t *__restrict gp, round_parameter_t *__restrict rp, side_parameter_t sp) {
    const int shift = 4 * (4 - sp.side);
    // for each available Ui,j / Vi,j
    const uint16_t sub_u = (rp->u >> shift) & 0xF;
    for (uint8_t sub_v = 0; sub_v <= 0xF; ++sub_v) {
        // calculate bias after select this case
        const double sub_bias = sub_u ? sp.prev_bias * gp->sbox_approx[sub_u][sub_v] / 8. : sp.prev_bias;
        // pruning if bias is too small
        if (sub_bias < gp->cut_bias) continue;

        const uint16_t new_v = sp.sub_v | (sub_v << shift);
        if (sp.side < 4) {
            // go to next S-Box step
            side_parameter_t n_sp = { .side = sp.side + 1, .sub_v = new_v, .prev_bias = sub_bias };
            sbox_assume(gp, rp, n_sp);
        } else {
            // go to next round step
            round_parameter_t n_rp = { .round = rp->round + 1, .u = new_v, .bias = sub_bias };
            round_assume(gp, n_rp);
        }
        // if Ui,j = 0 then explore just once; will be same for every case
        if (sub_u == 0) break;
    }
}

// Linear approximation for complete cipher
size_t full_approximation(approx_t *__restrict full_approx, int full_approx_size,
                          const int8_t (*__restrict sbox_approx)[0x10], double cut_bias, bool findK1) {
    // set appropriate S-Box approximation (approximation of K1 uses inverted S-Box; same with transposed)
    int8_t abs_sbox_approx[0x10][0x10];
    for (int i = 0; i < 0x10; ++i)
        for (int j = 0; j < 0x10; ++j)
            abs_sbox_approx[i][j] = abs(findK1 ? sbox_approx[j][i] : sbox_approx[i][j]);

    // set parameters
    global_parameter_t g_param = {
            .full_approx = full_approx, .full_approx_size = full_approx_size, .full_approx_count = 0,
            .sbox_approx = abs_sbox_approx, .cut_bias = cut_bias, .findK1 = findK1
    };

    // start round with every P mask (0 is ignored)
    for (int i = 1; i < 0x10000; ++i) {
        g_param.p = i;
        round_parameter_t r_param = { .round = 1, .u = g_param.p, 0.5 }; // bias will be 2^(n-1) * sub_biases
        round_assume(&g_param, r_param);
    }

    return g_param.full_approx_count;
}
