#include "crypt.h"
#include "approximation.h"
#include <stdio.h>

bool load_pairs(const char *filename, uint16_t (*pairs)[2], const int count, const uint16_t *key) {
    // check if file exists
    FILE *fp = fopen(filename, "r");
    if (!fp) return false;

    uint16_t k[5];
    if (5 != fscanf(fp, "%4hX %4hX %4hX %4hX %4hX", &k[0], &k[1], &k[2], &k[3], &k[4]) ||
        key[0] != k[0] || key[1] != k[1] || key[2] != k[2] || key[3] != k[3] || key[4] != k[4]) {
        fclose(fp);
        return false;
    }

    // load previous pairs and exit function
    for (int i = 0; i < count; ++i)
        if (2 != fscanf(fp, "%4hx %4hX", &pairs[i][0], &pairs[i][1])) {
            fclose(fp);
            return false;
        }

    fclose(fp);
    printf("%s loaded.\n", filename);
    return true;
}

void save_pairs(const char *filename, const uint16_t (*pairs)[2], const int count, const uint16_t *key) {
    FILE *fp = fopen(filename, "w");
    fprintf(fp, "%04hX %04hX %04hX %04hX %04hX\n", key[0], key[1], key[2], key[3], key[4]);
    for (int i = 0; i < count; ++i)
        fprintf(fp, "%04hx %04hX\n", pairs[i][0], pairs[i][1]);
    fclose(fp);
    printf("%s saved.\n", filename);
}

void save_sbox_approximation(const char *filename, int8_t approx[0x10][0x10]) {
    FILE *fp = fopen(filename, "w");
    for (int x_flag = 0x1; x_flag <= 0xF; ++x_flag) {
        for (int y_flag = 0x1; y_flag <= 0xF; ++y_flag)
            fprintf(fp, "%+hhd ", approx[x_flag][y_flag]);
        fprintf(fp, "\n");
    }
    fclose(fp);
    printf("%s saved.\n", filename);
}

size_t load_full_approximation(const char *filename, approx_t *full_approx, size_t full_approx_size) {
    // check if file exists
    FILE *fp = fopen(filename, "r");
    if (!fp) return false;

    for (size_t i = 0; i < full_approx_size; ++i)
        if (3 != fscanf(fp, "%4hx %4hX %f", &full_approx[i].x_mask, &full_approx[i].y_mask, &full_approx[i].bias)) {
            printf("%s loaded: %zu sets\n", filename, i);
            fclose(fp);
            return i;
        }

    fclose(fp);
    printf("%s loaded: %zu sets (overflow buffer size)\n", filename, full_approx_size);
    return full_approx_size;
}

void save_full_approximation(const char *filename, approx_t *full_approx, size_t count) {
    FILE *fp = fopen(filename, "w");
    for (size_t i = 0; i < count; ++i)
        fprintf(fp, "%04hx %04hX %lf\n", full_approx[i].x_mask, full_approx[i].y_mask, full_approx[i].bias);
    fclose(fp);
    printf("%s saved.\n", filename);
}

