#include "approximation.h"
#include "crypt.h"
#include <stdio.h>
#include <stdlib.h>

// Linear approximation of S-Box
void sbox_approximation(int8_t (*__restrict approx)[0x10]) {
    for (int x_mask = 0x1; x_mask <= 0xF; ++x_mask)
        for (int y_mask = 0x1; y_mask <= 0xF; ++y_mask) {
            approx[x_mask][y_mask] = -0x8; // values between [-8, +8]
            for (int m = 0x0; m <= 0xF; ++m)
                if (xor_bits(m & x_mask) == xor_bits(s_box[m] & y_mask))
                    approx[x_mask][y_mask]++;
        }
}

// print strong approximation.
void print_sbox_approximation(int8_t (*__restrict approx)[0x10], int level) {
    for (int x_mask = 0x1; x_mask <= 0xF; ++x_mask)
        for (int y_mask = 0x1; y_mask <= 0xF; ++y_mask)
            if (level <= abs(approx[x_mask][y_mask]))
                printf("strong approximation(%+d/16) %x -> %x\n", approx[x_mask][y_mask], x_mask, y_mask);
}
