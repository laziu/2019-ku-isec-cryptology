#include "crypt.h"
#include "approximation.h"
#include "attack.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// key!
static uint16_t key[5] = { 0x0123, 0x4567, 0x89AB, 0xCDEF, 0x0123 };

// pairs of plaintext and ciphertext
#define PAIR_COUNT 10000
static uint16_t pairs[PAIR_COUNT][2];

// approximation table of S-box
static int8_t sbox_approx[0x10][0x10];

// approximation table for complete cipher
static approx_t k5_approx[0x1000] = {
        { .x_mask = 0x00C0, .y_mask = 0xEEE0, .bias = 81.f / 1024.f },
        { .x_mask = 0x000A, .y_mask = 0x0777, .bias = 81.f / 1024.f },
};
static size_t k5_approx_size = 2;

static approx_t k1_approx[0x1000] = {
        { .x_mask = 0xA0A0, .y_mask = 0x0007, .bias = 0.046875 },
        { .x_mask = 0xCC00, .y_mask = 0x00E0, .bias = 0.046875 },
        { .x_mask = 0x4044, .y_mask = 0x0707, .bias = 0.0703125 },
};
static size_t k1_approx_size = 3;

// count subkey part
static inline void sampling(uint16_t sample_count[4][0x10], uint16_t sample_key, uint16_t mask) {
    for (int i = 0; i < 4; ++i)
        if (mask & (0x000Fu << ((3 - i) * 4)))
            sample_count[i][(sample_key >> ((3 - i) * 4)) & 0xF]++;
}

// estimate subkey from samples
static inline uint16_t estimate_key(uint16_t sample_count[4][0x10]) {
    uint16_t subkey[4][2] = {};
    for (size_t i = 0; i < 4; ++i)
        for (int j = 0; j <= 0xF; ++j)
            if (subkey[i][1] < sample_count[i][j]) {
                subkey[i][1] = sample_count[i][j];
                subkey[i][0] = j;
            }
    return (subkey[0][0] << 12) | (subkey[1][0] << 8) | (subkey[2][0] << 4) | subkey[3][0];
}

int main(void) {
    calc_inverses();

    // generate random pairs
//  if (!load_pairs("pairs.dat", pairs, PAIR_COUNT, key))  // load if dat.file exists
    {
        // generate pairs and write to file
        bool check[0x10000] = {};
        srand(time(0));
        for (int i = 0; i < PAIR_COUNT; ++i) {
            uint16_t value;
            while (check[value = (uint16_t) rand()]);
            check[value] = true;
            pairs[i][0] = value;
            pairs[i][1] = encrypt(value, key);
        }
        printf("new pairs generated.\n");
        save_pairs("pairs.dat", pairs, PAIR_COUNT, key);
    }

    // S-Box linear approximation
    sbox_approximation(sbox_approx);
    print_sbox_approximation(sbox_approx, 6);
//  save_sbox_approximation("approx.sbox.dat", sbox_approx);

    // generate linear approximation automatically
    if (false) {
        if (!(k5_approx_size = load_full_approximation("approx.k5.dat", k5_approx, 0x1000))) {
            k5_approx_size = full_approximation(k5_approx, 0x1000, sbox_approx, 0.07, false);
            save_full_approximation("approx.k5.dat", k5_approx, k5_approx_size);
        }
        if (!(k1_approx_size = load_full_approximation("approx.k1.dat", k1_approx, 0x1000))) {
            k1_approx_size = full_approximation(k1_approx, 0x1000, sbox_approx, 0.07, true);
            save_full_approximation("approx.k1.dat", k1_approx, k1_approx_size);
        }
    }

    // estimate K5 and K1
    uint16_t k5_sample[4][0x10] = {};
    for (size_t i = 0; i < k5_approx_size; ++i) {
        uint32_t result = attack_K5(k5_approx[i], pairs, PAIR_COUNT);
        sampling(k5_sample, result & 0xFFFFu, (result >> 16) & 0xFFFFu);
    }
    printf("K5 = %04X\n", estimate_key(k5_sample));

    uint16_t k1_sample[4][0x10] = {};
    for (size_t i = 0; i < k1_approx_size; ++i) {
        uint32_t result = attack_K1(k1_approx[i], pairs, PAIR_COUNT);
        sampling(k1_sample, result & 0xFFFFu, (result >> 16) & 0xFFFFu);
    }
    printf("K1 = %04X\n", estimate_key(k1_sample));

    return 0;
}
