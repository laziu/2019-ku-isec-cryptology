#ifndef MIDTERM_APPROXIMATION_H
#define MIDTERM_APPROXIMATION_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

// xor all bits
static inline uint8_t xor_bits(int bits) {
    uint8_t val = 0;
    for (; bits; val = !val)
        bits &= bits - 1; // least bit elimination
    return val;
}

// Linear approximation of S-Box
extern void sbox_approximation(int8_t approx[0x10][0x10]);

// print strong approximation.
extern void print_sbox_approximation(int8_t approx[0x10][0x10], int level);

// save S-Box approximation to file.
extern void save_sbox_approximation(const char *filename, int8_t approx[0x10][0x10]);

// type for express linear approximation
typedef struct approx_t {
    uint16_t x_mask;
    uint16_t y_mask;
    float bias; // data aligned size
} approx_t;

// Linear approximation for complete cipher
extern size_t full_approximation(approx_t *full_approx, int full_approx_size,
                                 const int8_t sbox_approx[0x10][0x10], double cut_bias, bool findK1);

// load linear approximation for complete cipher
extern size_t load_full_approximation(const char *filename, approx_t *full_approx, size_t full_approx_size);

// save linear approximation for complete cipher
extern void save_full_approximation(const char *filename, approx_t *full_approx, size_t count);

#endif //MIDTERM_APPROXIMATION_H
