#include "attack.h"
#include "approximation.h"
#include "crypt.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

uint32_t attack_K5(approx_t approx, const uint16_t (*__restrict pairs)[2], int count) {
    printf("Attack K5 with %04x|%04X (bias: %f) ... ", approx.x_mask, approx.y_mask, approx.bias);

    uint16_t best_key = 0;
    float best_bias = 1.f;

    // generate key
    volatile bool K1flag = false;
#pragma omp parallel for
    for (int K51 = 0; K51 <= 0xF000; K51 += 0x1000) { // uint16_t is overflowed; use int32
        if (K1flag) continue;
        for (uint16_t K52 = 0; K52 <= 0x0F00; K52 += 0x0100) {
            for (uint16_t K53 = 0; K53 <= 0x00F0; K53 += 0x0010) {
                for (uint16_t K54 = 0; K54 <= 0x000F; K54 += 0x0001) {
                    uint16_t K5 = (uint16_t) K51 | K52 | K53 | K54;

                    // calculate bias from pairs
                    int matches = 0;
                    for (int i = 0; i < count; ++i) {
                        uint16_t u_p = approx.x_mask & pairs[i][0];
                        uint16_t u_c = approx.y_mask & substitute(pairs[i][1] ^ K5, true);
                        if (xor_bits(u_p) == xor_bits(u_c))
                            matches++;
                    }
                    float bias = (float) abs(matches - (count / 2)) / count;

                    // replace if bias is better
                    if (best_bias > fabsf(bias - approx.bias)) {
                        best_bias = fabsf(bias - approx.bias);
                        best_key = K5;
                    }

                    if ((approx.y_mask & 0x000F) == 0) break;
                }
                if ((approx.y_mask & 0x00F0) == 0) break;
            }
            if ((approx.y_mask & 0x0F00) == 0) break;
        }
        if ((approx.y_mask & 0xF000) == 0) K1flag = true;
    }

    // print log
    printf("estimated K5 = ");
    for (int i = 3; i >= 0; --i) {
        if (approx.y_mask & (0x000Fu << (i * 4))) {
            printf("%X", (best_key >> (i * 4)) & 0x000Fu);
        } else
            putchar('-');
    }
    putchar('\n');

    // return (mask, key)
    return (((uint32_t) approx.y_mask) << 16) | best_key;
}

uint32_t attack_K1(approx_t approx, const uint16_t (*__restrict pairs)[2], int count) {
    printf("Attack K1 with %04x|%04X (bias: %f) ... ", approx.x_mask, approx.y_mask, approx.bias);

    uint16_t best_key = 0;
    float best_bias = 1.f;

    // generate key
    volatile bool K1flag = false;
#pragma omp parallel for
    for (int K11 = 0; K11 <= 0xF000; K11 += 0x1000) { // uint16_t is overflowed; use int32
        if (K1flag) continue;
        for (uint16_t K12 = 0; K12 <= 0x0F00; K12 += 0x0100) {
            for (uint16_t K13 = 0; K13 <= 0x00F0; K13 += 0x0010) {
                for (uint16_t K14 = 0; K14 <= 0x000F; K14 += 0x0001) {
                    uint16_t K1 = (uint16_t) K11 | K12 | K13 | K14;

                    // calculate bias from pairs
                    int matches = 0;
                    for (int i = 0; i < count; ++i) {
                        uint16_t u_p = approx.x_mask & substitute(pairs[i][0] ^ K1, false);
                        uint16_t u_c = approx.y_mask & pairs[i][1];
                        if (xor_bits(u_p) == xor_bits(u_c))
                            matches++;
                    }
                    float bias = (float) abs(matches - (count / 2)) / count;

                    // replace if bias is better
                    if (best_bias > fabsf(bias - approx.bias)) {
                        best_bias = fabsf(bias - approx.bias);
                        best_key = K1;
                    }

                    if ((approx.x_mask & 0x000F) == 0) break; // skip of part where key is not dependent
                }
                if ((approx.x_mask & 0x00F0) == 0) break;
            }
            if ((approx.x_mask & 0x0F00) == 0) break;
        }
        if ((approx.x_mask & 0xF000) == 0) K1flag = true;
    }

    // print log
    printf("estimated K1 = ");
    for (int i = 3; i >= 0; --i) {
        if (approx.x_mask & (0x000Fu << (i * 4)))
            printf("%X", (best_key >> (i * 4)) & 0x000Fu);
        else
            putchar('-');
    }
    putchar('\n');

    // return (mask, key)
    return (((uint32_t) approx.x_mask) << 16) | best_key;
}
