#include <stdio.h>
#include <stdint.h>

static inline uint64_t mod(int64_t a, uint64_t m) {
    while (a < 0) a += m;
    return a % m;
}

int64_t get_factor(uint64_t n) {
    if (n % 2 == 0) return 2;
    if (n % 3 == 0) return 3;
    for (int64_t i = 5; i * i <= n; i += 6) {
        if (n % i == 0) return i;
        if (n % (i + 2) == 0) return i + 2;
    }
    return -1;
}

uint64_t mod_inv(uint64_t b, uint64_t m) {
    int64_t a1 = 1, a2 = 0;
    uint64_t a3 = m;
    int64_t b1 = 0, b2 = 1;
    uint64_t b3 = b;
    while (1) {
        if (b3 == 0) return -1; // no inverse
        if (b3 == 1) return mod(b2, m);
        uint64_t q = a3 / b3;
        int64_t t1 = a1 - q * b1, t2 = a2 - q * b2;
        uint64_t t3 = a3 - q * b3;
        a1 = b1, a2 = b2, a3 = b3;
        b1 = t1, b2 = t2, b3 = t3;
    }
    return -1;
}

uint64_t mod_pow(uint64_t a, uint64_t n, uint64_t m) {
    if (a == 0) return 1;
    a %= m;
    n %= (m - 1);
    uint64_t x = a, y = 1;
    while (n > 1) {
        if (n % 2 == 1)
            y = (y * x) % m;
        x = (x * x) % m;
        n /= 2;
    }
    return (x * y) % m;

}

uint64_t decrypt_rsa(uint64_t c, uint64_t e, uint64_t p, uint64_t q) {
    uint64_t phi = (p - 1) * (q - 1);
    uint64_t d = mod_inv(e, phi);
    int64_t a_p = mod_pow(c, d, p);
    int64_t a_q = mod_pow(c, d, q);
    int64_t a_diff = mod(a_p - a_q, p);
    return a_q + ((a_diff * mod_inv(q, p)) % p) * q;
}

uint64_t decrypt_elgamal(uint64_t c1, uint64_t c2, uint64_t q, uint64_t a, uint64_t y_a) {
    for (uint64_t x_a = 2; x_a < q - 1; ++x_a) {
        if (y_a == mod_pow(a, x_a, q)) {
            uint64_t K = mod_pow(c1, x_a, q);
            return (c2 * mod_inv(K, q)) % q;
        }
    }
}

int main(void) {
    /*
    uint64_t n = 9943237852845877651u;
    uint64_t e = 13;

    uint64_t p = get_factor(n), q = n / p;

    uint64_t c[3] = {1220703125, 528567365900595529, 8792215503885098117};
    for (int i = 0; i < 3; ++i)
        printf("RSA C=%lld decrypt -> P=%lld\n", c[i], decrypt_rsa(c[i], e, p, q));
*/
    uint64_t ce[3][2] = {{2909170161, 2565161545},
                         {2909170161, 1004005362},
                         {2909170161, 2081016632}};
    for (int i = 0; i < 3; ++i)
        printf("ElGamal C=%lld,%lld decrypt -> P=%lld\n", ce[i][0], ce[i][1],
               decrypt_elgamal(ce[i][0], ce[i][1], 2934201397, 37, 2174919958));

    return 0;
}